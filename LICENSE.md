# LICENSE

Titanium Opensystems, lc License for the Software (as defined)
Copyright (c) 2018 Titanium Opensystems, lc
All rights reserved.

This license is a legal agreement between you and Titanium Opensystems, lc 
for the use of its Software (the "Software").  

By obtaining the Software you agree with the terms and conditions of this 
license.

PERMITTED USE
In the continent of North America ONLY:
You are permitted to use, copy, modify, and distribute the Software and its
documentation, with or without modification, for any purpose, provided that
the all the below conditions and promises are satisfied in full, otherwise
there is no license whatsoever:

The Software shall not be used, copied, modified, or distributed, outside 
the content of North America.

You agree to be bound by the laws of the United States and the Commonwealth 
of Virginia, without regard to law of conflicts of law. You agree to exclusive 
jurisdiction and venue of courts located in the Commonwealth of Virginia

A copy of this license agreement must be included with the distribution.

Redistributions of source code must retain the above copyright notice 
together with all source code files.

Redistributions in binary form must reproduce the above copyright notice
in the documentation and/or other materials provided with the distribution.

Any files that have been modified must carry notices stating the nature
of the change and the names of those who changed them.

Products derived from the Software must include an acknowledgment that
they are derived from the Software in their documentation and/or other
materials provided with the distribution.

INDEMNITY
You agree to indemnify and hold harmless the authors of the Software and
any contributors for any direct, indirect, incidental, or consequential
third-party claims, actions or suits, as well as any related expenses,
liabilities, damages, settlements or fees arising from your use or misuse
of the Software, or a violation of any terms of this license.

DISCLAIMER OF WARRANTY
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR
IMPLIED, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF QUALITY, PERFORMANCE,
NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.

LIMITATIONS OF LIABILITY
YOU ASSUME ALL RISK ASSOCIATED WITH THE INSTALLATION AND USE OF THE SOFTWARE.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS OF THE SOFTWARE BE LIABLE
FOR CLAIMS, DAMAGES OR OTHER LIABILITY ARISING FROM, OUT OF, OR IN CONNECTION
WITH THE SOFTWARE. LICENSE HOLDERS ARE SOLELY RESPONSIBLE FOR DETERMINING THE
APPROPRIATENESS OF USE AND ASSUME ALL RISKS ASSOCIATED WITH ITS USE, INCLUDING
BUT NOT LIMITED TO THE RISKS OF PROGRAM ERRORS, DAMAGE TO EQUIPMENT, LOSS OF
DATA OR SOFTWARE PROGRAMS, OR UNAVAILABILITY OR INTERRUPTION OF OPERATIONS.

