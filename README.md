*CIM-ACCELERANT (A.K.A. CIM-XA)*
==============================

A CI-2 based distribution (Authored and released by Titanium Opensystems, l.c.) including the popular CodeIgniter framework (Version 2.x) and many extensions. As usual, most of the action can be found in /application/controllers and /application/models.
You may find this distribution helpful if your situation includes one or more of the following:

- You are maintaining legacy Codeigniter 2 applications.
- You need PDO for cross-DBMS compatibility (Idiorm)
- You need easy-to-use support for SQL prepared statements (Idiorm)
- You need or simply prefer a more mainstream ORM approach (Idiorm)
- You need a fully RESTful server implementation capability
- You need views/templates that auto-escape output (Savant3)
- You need login/authorization (a provided CI example or hybridauth)
- You need a job queue facility (Celery + AmqPHP)
- You need a cURL library (PHPcURL)
- You need to format HTML email (Savant)


Intro
-----------------

This distribution includes zero-compilation, object-oriented controllers, models, and views.

This distribution stands on the shoulders of some PHP giants including Ellis Labs (CodeIgniter), Jamie Matthews (Idiorm), 
Paul M. Jones (Savant), Alex Bilbie (MongoDB library) and others. 

Some Useful External Resources
-----------------

<a href="http://ellislab.com/codeigniter/user-guide/toc.html">Documentation for CodeIgniter</a>

<a href="https://github.com/philsturgeon/codeigniter-restserver">RESTful Server Tutorial for CodeIgniter by Phil Sturgeon</a>

<a href="http://idiorm.readthedocs.org/en/latest/">Documentation for Idiorm Object Relational Mapper; and </a>

For (optional) Savant Templating</a> see: <a href="http://devzone.zend.com/1542/creating-modular-template-based-interfaces-with-savant/">this article</a>. 

While Savant is included in this distribution, it is not mandatory to use it for presentation -- CodeIgniter views may be used. 
Job queue functionality requires Python and is based on the following projeects: 
<a href="https://github.com/hussaintamboli/Celery-CI">Celery-CI</a> (housed in /application/libraries)
<a href="https://github.com/gjedeer/celery-php">Celery-PHP</a> 
<a href="http://www.php.net/manual/en/amqp.setup.php">AMQP</a> 
<a href="http://www.toforge.com/2011/01/run-celery-tasks-from-php/">Run Celery Tasks Article</a>

Installation Notes
-----

Libraries are housed in / or in /application/libraries, except as noted in the various README files in /. 
This distribution's configuration for CodeIgniter departs from the original as follows: 
- index_page is set to: ' ' to support URLs without showing index.php and .htaccess is coded accordingly so Apache must 
have rewrite set up. 
- there is an optional auth system, using MY_Bouncer in application/core, which is bypassed for any class that directly extends CI -- ie: doesn't 
extend MY-Bouncer. 

Other considerations:
- If you use CI's Encryption class or CI's Session class you MUST set an encryption key.  See application/config/config.php
- this distribution's Savant departs from the original in that it assumes the path to templates is set to application/templates 
and the sample code reflects this assumption. 
- a database connection string configuration for Idiorm is located in application/config/idiorm.php which you may include in your controller constructor, otherwise Idiom expects it coded inline. This string must reflect your database server attributes as described in the Idiorm documentation; to use CI database access features requires defining a database connection in application/config/database.php.
 
Sample Code
-----------
THERE ARE SMALL SAMPLE CONTROLLERS in application/controllers/ showing several forms of usage. The song.php and songs*.php series of sample controllers illustrate two routing and classfile alternative approaches. There is also a sample controller for job queues in application/controllers/jobq.php. DBscripts/test.sql contains some sample data--enough to run many (but not all) of the samples.

LICENSE AND EXPORT NOTICE
-------------------------
**THIS DISTRIBUTION IS LICENSED AS A COMPILATION WORK UNDER THE SAME TERMS AS SET FORTH IN 
<A HREF="http://ellislab.com/codeigniter/user-guide/license.html">THE CODEIGNITER LICENSE</A>. THE INDIVIDUAL COMPONENTS ARE SUBJECT TO THEIR 
RESPECTIVE LICENSES. ALL TRADEMARKS ARE THE PROPERTY OF THEIR RESPECTIVE OWNERS AND TITANIUM OPENSYSTEMS HAS NO AFFILIATION WITH ANY OF THEM. 
BY USING THIS DISTRIBUTION IN WHOLE OR PART YOU CONSENT TO ALL APPLICABLE LICENSE TERMS, INCLUDING WITHOUT LIMITATION, THIS PARAGRAPH.**

NOTWITHSTANDING ANY OTHER PROVISION OF THE CONTENTS INCLUDED IN OR REFERENCED BY THIS REPOSITORY: 
By downloading this software, you acknowledge that you understand all of the following: this software and technical information may be subject to the U.S. Export Administration Regulations (the "EAR") and other U.S. and foreign laws and may not be exported, re-exported or transferred (a) to any country listed in Country Group E:1 in Supplement No. 1 to part 740 of the EAR (currently, Cuba, Iran, North Korea, Sudan & Syria); (b) to any prohibited destination or to any end user who has been prohibited from participating in U.S. export transactions by any federal agency of the U.S. government; or (c) for use in connection with the design, development or production of nuclear, chemical or biological weapons, or rocket systems, space launch vehicles, or sounding rockets, or unmanned air vehicle systems. You may not download this software or technical information if you are located in one of these countries or otherwise subject to these restrictions. You may not provide this software or technical information to individuals or entities located in one of these countries or otherwise subject to these restrictions. You are also responsible for compliance with foreign law requirements applicable to the import, export and use of this software and technical information.



