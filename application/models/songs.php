<?php
class songs extends CI_Model  {

function i_getS() {
	require 'idiorm/idiorm.php';
	require 'application/config/idiorm.php';
	require 'savant/Savant3.php'; 
	$data = new Savant3(); //data object will be instance of savant3
	$data->songs = ORM::forTable('songs') 
		->whereRaw('(`time` > ? AND `time` < ?)', array(2, 6)) 
		->orderByAsc('artist') 
		->findResultSet(); 
	return($data);
}

function i_get() {
	require 'idiorm/idiorm.php';
	require 'application/config/idiorm.php';
	$data = new stdClass; //data object will be instance of stdClass in lieu of a custom class
	$data->songs = ORM::forTable('songs') 
		->whereRaw('(`time` > ? AND `time` < ?)', array(2, 6)) 
		->orderByAsc('artist') 
		->findResultSet(); 
	return($data);
}     	     	
/*
function ci_get() {
	$where = 'time > 2 AND time < 6';
	$this->db->from('songs')
		->where($where)
		->order_by('artist');
	$data = new stdClass;
	$data->songs = $this->db->get()->result(); 
	return($data);
}     	     	
*/
function ci_get() {
	$where = 'time > 2 AND time < 6';
	$data = new stdClass;
	$data->songs = $this->db->from('songs')->where($where)->order_by('artist')->get()->result(); 
	return($data);
}                	

function ci_getS() {
	require 'savant/Savant3.php'; 
	$where = 'time > 2 AND time < 6';
//	$this->db->from('songs')
//		->where($where)
//		->order_by('artist');
	$data = new Savant3(); //data object will be instance of savant3
//	$data->songs = $this->db->get()->result(); 
	$data->songs = $this->db->from('songs')->where($where)->order_by('artist')->get()->result(); 
	return($data);
}     

} 
