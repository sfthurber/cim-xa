<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="//fonts.googleapis.com/css?family=Lobster&amp;subset=latin" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
<style>
div { display:none; }
.header { font-family:lobster; width:600px; padding-bottom:6px; padding-top:7px; font-size:24px; background:#3276B1; color:white; }
.center { text-align: center; margin-left: auto; margin-right: auto; }
.form-horizontal { width:600px; border: 1px solid #3276B1; padding-top:20px; }
.form-control { color:#555; }
</style>	
</head>
<body>
<div style='height:40px;'></div>  
<div class="center header" style=''>Titanium Opensystems</div>  
<div>
<div class="form-horizontal center">
		
		<a href="<?=site_url()?>song/songs2c/">CIMXA Songs--CI model with a trivial CI view</a><br />
		<a href="<?=site_url()?>song/songs1c/">CIMXA Songs--CI model with a trivial Savant view</a><br />
		<a href="<?=site_url()?>song/songs2/">CIMXA Songs--Idiorm model with a trivial CI view</a><br />
		<a href="<?=site_url()?>song/songs1/">CIMXA Songs--Idiorm model with a trivial Savant view</a><br />
		
</div>
</div>
</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>$(document).ready(function(){ $('div').fadeIn(1000); });</script>	
</body>
</html>
