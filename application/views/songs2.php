<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="x-ua-compatible" content="IE=9" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
<style>
.container { 
width: 100% !important;
}
.center {
text-align: center;
margin-left: auto;
margin-right: auto;
}
</style>	
</head>
<body>
<!-- CODEIGNITER PRESENTATION LAYER WITH escecho FUNCTION-->
<?php function escecho($echoitem){echo htmlentities($echoitem, ENT_QUOTES);} //for output code a cleaner look?>
<div class="container center">
<table class="table">
<?php foreach ($songs as $song): ?> 
<tr>
	<td>ARTIST:</td>
	<td><?php escecho($song->artist)?></td>
	<td>TITLE:</td>
	<td><?php escecho($song->title)?></td>
	<td>TIME:</td>
	<td><?php escecho($song->time)?></td>
</tr>
<?php endforeach; ?>
</table>
</div>
</body>
