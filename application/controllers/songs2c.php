<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Songs2c extends CI_Controller {

//CIMXA Songs--CI model with a trivial CI view

public function exec() 

{
	$this->load->model('songs');	
	$this->load->view('songs2', $this->songs->ci_get());
}

} // END Class
