<?php 

class Song extends CI_Controller {
//assumes database library is autoloaded	 
function __construct() 
{
	parent::__construct(); 		
	$this->load->model('songs');
}	

public function songs1() //idiom model savant view engine

{
	$this->songs->i_getS()->display('songs1.php'); 
	// savant looks to: application/templates/songs1.php 
} 

public function songs1c() //CI model savant view engine

{
	$this->songs->ci_getS()->display('songs1.php'); 
	// savant looks to: application/templates/songs1.php 
} 

public function songs2() //idiorm model CI view engine

{
	$this->load->view('songs2', $this->songs->i_get());
}   

public function songs2c() // CI model CI view engine

{
	$this->load->view('songs2', $this->songs->ci_get());
}


 public function songs3() // Grocery CRUD

{
//autoloaded	$this->load->database();
//autoloaded	$this->load->library('grocery_CRUD');	
	$crud = new grocery_CRUD();
	$this->load->view('songs3.php', $crud->set_table('songs')->unset_export()->unset_print()->render());  
}   

} 
